import sys
import os.path
import os
import shutil

def check_args():
    if (len(sys.argv) != 3):
        print("Incorrect number of argument: Expected 3. Found:" + str(len(sys.argv)), file=sys.stderr)
        return False
    if not os.path.isdir(sys.argv[1]):
        print ("Cannot find directory " + sys.argv[1])
        return False
    return True

def check_hidden_directories(given_path):
    git = os.path.join(given_path, '.git')
    vs = os.path.join(given_path, '.vs')
    clean = True
    if os.path.isdir(git):
        print(".git directory has been found. Please delete it")
        clean = False
    if os.path.isdir(vs):
        print(".vs directory has been found. Please delete it")
        clean = False
    return clean

def remove_bin_obj(given_path):
    directory = os.listdir(given_path)
    for name in directory:
        if not name.startswith('.'): # To do not check hidden directory like git and go directly into each project directory
            project = os.path.join(given_path, name)
            if os.path.isdir(project):
                for sub_directory in os.listdir(project):
                    if (sub_directory == 'bin' or sub_directory == 'obj'):
                        path = os.path.join(project, sub_directory)
                        try:
                            shutil.rmtree(path)
                            print('DELETED: ' + path)
                        except:
                            print("Error happened when removing: %s" % (path))

def main():
    if not check_args():
        return 1
    GIVEN_DIR = sys.argv[1]
    ARCHIVE_NAME = sys.argv[2]
    if not check_hidden_directories(GIVEN_DIR):
        return 2
    remove_bin_obj(GIVEN_DIR)
    try:
        shutil.make_archive(ARCHIVE_NAME, 'zip', sys.argv[1])
    except:
        print("An error happened when trying to zip the directory")
    print("Created " + ARCHIVE_NAME + '.zip')

if __name__ == "__main__":
    main()