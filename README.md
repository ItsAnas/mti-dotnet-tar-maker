# mti DotNet Tar Maker

Simple cleaner and zip maker that respect the architecture
need in .Net tutorials given by Arnaud Lemettre.

## Disclaimer

Do not justify your fail on a project with this script.
You are the only one responsible.
Please, check every zip before sending it.

### What does it do ?

It simply warn you that you have some trash directory like .git
or .vs created respectively by git and visual studio.

It also **delete** every directory **bin** and **obj** from each 
project.

### Usage:

```
python ./script.py path_to_csharp_project archive_name
```

### Example:

```
python ./script ..\..\Documents\Projects\School\mti-dot-net\dlyoutube login_lDLYoutube
```

will delete bin and obj from each sub-project and create an zip of 
the c# project named **login_lDLYoutube.zip**.

### Author & Contributor:

Anas El halouani 'el_hal_a' 2021